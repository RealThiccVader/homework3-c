#include <stdio.h>
#include <string.h>
#include <stdbool.h>

// Elevator starts at G

// User input to selected floor

// Limit the selected floors

// Discard wrong enteries and ask user again

// User decided to leave end program

// User decided to move again change current floor

// Defining global variables
#define FLOOR_STRING_LENGTH 9
#define FLOOR_ELEMENT_LENGTH 5

// Setting up typedef for easier use
typedef char* String;
typedef void* V0idPointer;

// This can be done without pointers as strings are pointers. But i used them to show pointers.
bool TakeInput(char floors[][FLOOR_ELEMENT_LENGTH], String* currentFloor);
void visuals(char floors[][FLOOR_ELEMENT_LENGTH], String* currentFloor);

int main() {
    // Elevator starts at G
    // From assigment To Do List from class on date Dec 22, 2022
    // Variable floor holds all the floor levels in type string(char array) that is inside a char array
    char floors[FLOOR_STRING_LENGTH][FLOOR_ELEMENT_LENGTH] = {"B3","B2","B1","G", "1", "2", "3", "4", "5"};
    char currentFloor[FLOOR_ELEMENT_LENGTH] = {0};
    bool readyToQuit = false;
    //Pointer to currentFloor showing pointers from class on date Dec 12, 2022
    String ptrCF = currentFloor;

    // Copies the string "G" into currentFloor
    strcpy(currentFloor,"G");

    // Continue to work until user inputs nothing
    while(!readyToQuit) {
        readyToQuit = TakeInput(floors, &ptrCF);
    }
    return 0;
}

// User input to selected floor
bool TakeInput(char floors[][FLOOR_ELEMENT_LENGTH], String* currentFloor) {
    printf("\nInput: ");
    char input[FLOOR_ELEMENT_LENGTH] = {0};
    // Variable exists is there to check if user input is an option from the floors array
    bool exists = false;
    char tmp[FLOOR_ELEMENT_LENGTH] = {0};

    // I decided against fgets as when comparing string it will be for 10 larger as the new line character is saved, and in this case we dont need it
    gets(input);

    // Return true if string doesnt have a length therefore its just a new line so we can stop the program
    if(!strlen(input)) return true;

    // Optional: you can enter small letters for basment floors
    for (int i = 0; i < sizeof(input); i++) {
        char c = input[i];
        if(c>='a' && c<='z'){
            input[i] -= 32;
        }
    }
    

    // Limit the selected floors
    for (int i = 0; i < FLOOR_STRING_LENGTH; i++) {
        // Compares input to current floor iteration
        if (strcmp(input, floors[i]) == 0) {
            exists = true;
        }
    }

    sscanf(input, "%s", tmp);
    if (exists == true && strcmp(tmp, *currentFloor) != 0) {
        strcpy(*currentFloor, tmp);
        // Calls function visuals to render the ccurrent floor options
        visuals(floors, currentFloor);
    }
    else {
        // Checks if user is already on this floor
        if (strcmp(input, *currentFloor) == 0) {
            printf("You are already on this floor!\n");
        }
        // Takes into account wrong user input
        else {
            printf("Wrong input you must select one of the specified floors!\n");
        }

        // Starts again since the user did something bad
        TakeInput(floors, currentFloor);
    }
    return false;
}

void visuals(char floors[][FLOOR_ELEMENT_LENGTH], String* currentFloor) {
    printf("Available floors : \n");

    for (int i = 0; i < FLOOR_STRING_LENGTH; i++) {
        if (strcmp(floors[i], *currentFloor) != 0) {
            printf("%s \n", floors[i]);
        }
    }
}