#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

#define WORD_ELEMENT_LENGTH 10

typedef char* String;
bool takeInput(String secrete, String guess, int* failed, String used);
void visuals(String secrete, int* failed);

int main() {
    // Random numbers every time the program is started (by computer time)
    srand(time(0));

    int number = (rand() % (20 - 0 + 1)) + 0;
    char pickArray[30][WORD_ELEMENT_LENGTH] = {
        "SWIFT",
        "SWARM",
        "CLANK",
        "FLOOD",
        "SPLAY",
        "BISIE",
        "REDUB",
        "AGREE",
        "GRIND",
        "STEAM",
        "PULSE",
        "STUMP",
        "SCOOT",
        "EXCEL",
        "WHIRL",
        "SPOCK",
        "PROKE",
        "SPADE",
        "WAIVE",
        "SLOPE"
    };
    char secrete[WORD_ELEMENT_LENGTH] = {0};
    char guess[WORD_ELEMENT_LENGTH] = {0};
    char used[30] = {0};
    int failed = 0;
    bool readyToQuit = false;

    // Copying values into the variables
    strcpy(secrete,pickArray[number]);
    strcpy(guess,"_____");

    // Setting up pointers
    int* failedPtr = &failed;
    String usedPtr = used;
    
    // Checking if the program has ended since takeInput is boolean
    while (!readyToQuit)
    {
        // If more than 6 times the letter is a bust you fail
        if(*failedPtr>=6){
            printf("FAIL!");
            printf("The word was: %s",pickArray[number]);
            return 0;
        }else{
            // Store if the game is a fail in readyToQuit
            readyToQuit = takeInput(secrete, guess, failedPtr, usedPtr);
            visuals(secrete, &failed);
            if(readyToQuit){
                printf("\n\nCONGRATULATIONS!\n\n");
            }
        }
    }

    return 0;
}


bool takeInput(String secrete, String guess, int* failed, String used) {
    printf("\n\n\nInput: ");
    char input;
    bool exists = false;
    char tmp[WORD_ELEMENT_LENGTH] = {0};

    gets(&input);

    // Optional: you can enter small letters for letters
    char c = input;
    if(c>='a' && c<='z'){
        input -= 32;
    }
    
    // Copying values into the variables
    strcpy(tmp,secrete);
    strncat(used, &input,1);
    printf("\nUsed so far: %s\n", used);

    for (int i = 0; i < WORD_ELEMENT_LENGTH; i++) {
        // Compares input to current letter in itteration
        if (input == tmp[i]) {
            // If there is a letter in the word same as input say it exists
            exists = true;
            guess[i]=input;

        }
    }

    printf("\n%s \n\n",guess);
    if(exists==false){
        *failed+=1;
    }

    //Checks if the game has ended comapring the word with the urrent guess
    if(strcmp(guess,secrete)==0){
        return true;
    }
    return false;
}
void visuals(String secrete, int* failed){
    char stickMan[6][20] = {"   @ \n","  /","|","\\ \n","  /"," \\ \n\n"};
    // printf("  @ \n");
    // printf(" /|\\ \n");
    // printf(" / \\ \n");
    printf("\n---|\n");
    // Printing everything with a for to keep it nicer
    for (int i = 0; i < *failed; i++)
    {
        printf("%s",stickMan[i]);
    }
    
}
